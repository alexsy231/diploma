/* Scroll */


window.onscroll = function () {
    scrollFunction()
};
var navbar = document.getElementById('menu');
var fixed = navbar.offsetTop;

function scrollFunction() {
    if (window.pageYOffset >= fixed) {
        navbar.classList.add('fixed');
    } else {
        navbar.classList.remove('fixed');
    }
}

/* Active class menu */

var menu = document.getElementById('menu');
var link = menu.getElementsByClassName('header-menu__link');
for (var i = 0; i < link.length; i++) {
    link[i].addEventListener('click', function () {
        var current = document.getElementsByClassName('actives');
        current[0].className = current[0].className.replace(' actives', "");
        this.className += ' actives';
    });
}
/* Anchors */

var anchors = document.querySelectorAll('a[href*="#"]')

for (let anchor of anchors) {
    anchor.addEventListener('click', function (event) {
        event.preventDefault();
        const blockID = anchor.getAttribute('href')
        document.querySelector('' + blockID).scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        });
    });
};


/* Menu nav click */
document.querySelector('.header-hamburger').addEventListener('click', function (e) {
    e.preventDefault();
    this.classList.toggle('is-active');
})

/* Menu show and hide */
window.onload = function () {
    var menuStyle = getComputedStyle(menu);
    toggleMenu.onclick = function () {
        if (menuStyle.display == 'none') {
            menu.classList.add('active');
        } else {
            menu.classList.remove('active');
        }
    }
}
/* Accordion */

var accordion = document.getElementsByClassName('accordion-header'),
    contain = document.getElementsByClassName('accordion-item__content');

for (var i = 0; i < accordion.length; i++) {
    accordion[i].addEventListener('click', function () {
        if (!(this.classList.contains('active'))) {
            for (var i = 0; i < accordion.length; i++) {
                accordion[i].classList.remove('active');
            }
            this.classList.add('active');
        };

    });
};

/* Quotes slider */


var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName('slider');
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = 'none';
    }
    slides[slideIndex - 1].style.display = 'block';
}

/* Quotes slider 2 */

var slide2Index = 1;
showSlides2(slide2Index);

function plusSlides2(n) {
    showSlides2(slide2Index += n);
}

function currentSlide2(n) {
    showSlide2s(slide2Index = n);
}

function showSlides2(n) {
    var i;
    var slides2 = document.getElementsByClassName('slider2');
    if (n > slides2.length) {
        slide2Index = 1
    }
    if (n < 1) {
        slide2Index = slides2.length
    }
    for (i = 0; i < slides2.length; i++) {
        slides2[i].style.display = 'none';
    }
    slides2[slide2Index - 1].style.display = 'block';
}


/* Modal img */

var modal = document.getElementById('myModal');
var modal2 = document.getElementById('myModal2');
var modal3 = document.getElementById('myModal3');
var modal4 = document.getElementById('myModal4');
var modal5 = document.getElementById('myModal5');
var modal6 = document.getElementById('myModal6');
var modal7 = document.getElementById('myModal7');

var img = document.getElementById('myImg');
var img2 = document.getElementById('myImg2');
var img3 = document.getElementById('myImg3');
var img4 = document.getElementById('myImg4');
var img5 = document.getElementById('myImg5');
var img6 = document.getElementById('myImg6');
var img7 = document.getElementById('myImg7');


var modalImg = document.getElementById('img01');
var modalImg2 = document.getElementById('img02');
var modalImg3 = document.getElementById('img03');
var modalImg4 = document.getElementById('img04');
var modalImg5 = document.getElementById('img05');
var modalImg6 = document.getElementById('img06');
var modalImg7 = document.getElementById('img07');

img.onclick = function () {
    modal.style.display = 'block';
    modalImg.src = this.src;
}

img2.onclick = function () {
    modal2.style.display = 'block';
    modalImg2.src = this.src;
}

img3.onclick = function () {
    modal3.style.display = 'block';
    modalImg3.src = this.src;
}

img4.onclick = function () {
    modal4.style.display = 'block';
    modalImg4.src = this.src;
}
img5.onclick = function () {
    modal5.style.display = 'block';
    modalImg5.src = this.src;
}
img6.onclick = function () {
    modal6.style.display = 'block';
    modalImg6.src = this.src;
}
img7.onclick = function () {
    modal7.style.display = 'block';
    modalImg7.src = this.src;
}
var span = document.getElementsByClassName('close')[0];
var span2 = document.getElementsByClassName('close')[1];
var span3 = document.getElementsByClassName('close')[2];
var span4 = document.getElementsByClassName('close')[3];
var span5 = document.getElementsByClassName('close')[4];
var span6 = document.getElementsByClassName('close')[5];
var span7 = document.getElementsByClassName('close')[6];

span.onclick = function () {
    modal.style.display = 'none';
}
span2.onclick = function () {
    modal2.style.display = 'none';
}
span3.onclick = function () {
    modal3.style.display = 'none';
}

span4.onclick = function () {
    modal4.style.display = 'none';
}
span5.onclick = function () {
    modal5.style.display = 'none';
}
span6.onclick = function () {
    modal6.style.display = 'none';
}
span7.onclick = function () {
    modal7.style.display = "none";
}

/* ContactForm */

var input = document.getElementById('contactForm');
input.addEventListener('keyup', function (event) {
    event.preventDefault();
    if (event.keyCode === 13) {
        document.getElementById('formBtn').click();
    }
});


/* Footer instagram img choice */

var imageArray = document.getElementsByClassName('footer-instagram__img');
var isHidden = false;

function selectimage(item) {
    for (var i = 0; i < imageArray.length; i++) {
        imageArray[i].style.display = isHidden ? 'block' : 'none';
    }

    isHidden = !isHidden;
    item.style.display = 'block';
}