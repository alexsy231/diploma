const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify-es').default;
const del = require('del').default;
const browserSync = require('browser-sync').create();




function html() {
    return gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./build'));
}


function image() {
    return gulp.src('./src/img/**/*.*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{
                removeViewBox: false
            }],
            interlaced: true
        }))
        .pipe(gulp.dest('./build/img'));
}


function styles() {
    return gulp.src('./src/styles/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('main.css'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./build/styles'))
        .pipe(browserSync.stream());

}

function scripts() {
    return gulp.src('./src/scripts/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('./build/scripts'))
        .pipe(browserSync.stream());



}

function watch() {
    browserSync.init({
        server: {
            baseDir: './src/'
        }
    });
    gulp.watch('./src/styles/**/*/.scss', styles);
    gulp.watch('./src/scripts/**/*/.js', scripts);
    gulp.watch('./*.html', browserSync.reload);

}

function clean() {
    return del(['build/*'])
}


gulp.task('image', image)
gulp.task('html', html);
gulp.task('styles', styles);
gulp.task('scripts', scripts);
gulp.task('watch', watch);

gulp.task('build', gulp.series(clean,
    gulp.parallel(styles, scripts, html, image)
));


